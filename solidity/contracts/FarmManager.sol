// SPDX-License-Identifier: MIT
pragma solidity ^0.8.0;

import "@openzeppelin/contracts-upgradeable/access/OwnableUpgradeable.sol";
import "@openzeppelin/contracts/token/ERC20/ERC20.sol";

// const c = (await ethers.getContractFactory("FarmManager")).attach("0x977D9486888dE0Fe055bdbF404CbCd6a20a30585");
// await c.addFarm("0x8babbb98678facc7342735486c851abd7a0d17ca", ethers.BigNumber.from(5).mul(ethers.BigNumber.from(10).pow(18)).div(ethers.BigNumber.from(100000)), ethers.BigNumber.from(4000).mul(ethers.BigNumber.from(10).pow(18)), 3600 * 24, true)
// await c.addFarm("0xed24fc36d5ee211ea25a80239fb8c4cfd80f12ee", ethers.BigNumber.from(5).mul(ethers.BigNumber.from(10).pow(18)).div(ethers.BigNumber.from(10000)), ethers.BigNumber.from(3000).mul(ethers.BigNumber.from(10).pow(18)), 3600 * 24, true)
// await c.addFarm("0xfd336aa8fa9715d2ce259ad326e04d2221059614", ethers.BigNumber.from(5).mul(ethers.BigNumber.from(10).pow(18)).div(ethers.BigNumber.from(1000)), ethers.BigNumber.from(2000).mul(ethers.BigNumber.from(10).pow(18)), 3600 * 24, true)
// await c.fund(ethers.BigNumber.from(5000).mul(ethers.BigNumber.from(10).pow(18)).div(ethers.BigNumber.from(100)), {gasPrice: ethers.utils.parseUnits('10', 'gwei'), gasLimit: 1000000})

// await c.deposited(0, "0x7a964F429954de9772F7179390416098903eC8A0")
// await c.pending(0, "0x7a964F429954de9772F7179390416098903eC8A0")
// await c.deposit(0, ethers.BigNumber.from(50).mul(ethers.BigNumber.from(10).pow(18)).div(ethers.BigNumber.from(100)), {})
// await c.withdraw(0, ethers.BigNumber.from('0x06f05b59d3b20000'), {gasPrice: ethers.utils.parseUnits('10', 'gwei'), gasLimit: 1000000})

contract FarmManager is OwnableUpgradeable {
    // Info of each user.
    struct User {
        uint timestamp;
        uint256 amount;     // How many LP tokens the user has provided.
        uint256 rewardDebt; // Reward debt. See explanation below.
        //
        // We do some fancy math here. Basically, any point in time, the amount of ERC20s
        // entitled to a user but is pending to be distributed is:
        //
        //   pending reward = (user.amount * farm.accERC20PerShare) - user.rewardDebt
        //
        // Whenever a user deposits or withdraws LP tokens to a farm. Here's what happens:
        //   1. The farm's `accERC20PerShare` (and `lastRewardBlock`) gets updated.
        //   2. User receives the pending reward sent to his/her address.
        //   3. User's `amount` gets updated.
        //   4. User's `rewardDebt` gets updated.
    }

    // Info of each farm.
    struct Farm {
        ERC20 depositToken;             // Address of LP token contract.
        uint256 allocPoint;         // How many allocation points assigned to this farm. ERC20s to distribute per block.
        uint256 lastRewardBlock;    // Last block number that ERC20s distribution occurs.
        uint256 accERC20PerShare;   // Accumulated ERC20s per share, times 1e36.
        uint256 lockTime;
        uint256 blockFrom;
        uint256 blockTo;
        uint256 paidOut;
        uint256 rewardPerBlock;
    }

    struct FarmMeta {
        Farm farm;
        uint256 farmId;
        uint256 progress;
        uint256 deposited;
        uint256 balance;
        uint256 supply;
        string tokenSymbol;
        string tokenName;
        uint8 tokenDecimals;
        address tokenAddress;
        uint256 unlockTime;
        uint256 pendingReward;
        uint256 remainReward;
    }

    struct FarmMetaInfo {
        uint256 totalSupply;
        uint256 currentSupply;
        uint256 rewardPerBlock;
        uint256 farmCount;
        uint256 tokenPrice;
    }

    IERC20 public rewardToken;
    Farm[] public farms;
    mapping (uint256 => mapping (address => User)) public users;
    uint256 public totalAllocPoint;

    event Deposit(address user, uint256 farmId, uint256 amount);
    event Withdraw(address user, uint256 farmId, uint256 amount);

    function initialize() initializer public {
        __Ownable_init();

        rewardToken = ERC20(0x8F5393F5c4727DBa352cab9B794b00E8e44A6F98);
        totalAllocPoint = 0;
    }

    function farmsCount() external view returns (uint256) {
        return farms.length;
    }

    // Fund the farm, increase the end block
    function fund(uint256 farmId, uint256 value) public {
        Farm storage farm = farms[farmId];

        if (farm.blockTo > 0) {
            require(block.number < farm.blockTo, "fund: too late, the farm is closed");
        }

        rewardToken.transferFrom(address(msg.sender), address(this), value);
        farm.blockTo += value / farm.rewardPerBlock;
    }

    // Add a new token to the farm. Can only be called by the owner.
    // DO NOT add the same LP token more than once. Rewards will be messed up if you do.
    function addFarm(
        ERC20 depositToken,
        uint256 rewardPerBlock,
        uint256 allocPoint,
        uint256 lockTime,
        bool withUpdate
    ) public onlyOwner {
        if (withUpdate) updateAllFarms();

        uint256 blockFrom = block.number;
        uint256 blockTo = blockFrom + (allocPoint / rewardPerBlock);
        uint256 lastRewardBlock = blockFrom;

        rewardToken.transferFrom(address(msg.sender), address(this), allocPoint);

        totalAllocPoint = totalAllocPoint + allocPoint;
        farms.push(Farm({
            depositToken: depositToken,
            allocPoint: allocPoint,
            lastRewardBlock: lastRewardBlock,
            accERC20PerShare: 0,
            lockTime: lockTime,
            blockFrom: blockFrom,
            blockTo: blockTo,
            paidOut: 0,
            rewardPerBlock: rewardPerBlock
        }));
    }

    // Update the given farm's ERC20 allocation point. Can only be called by the owner.
    function setFarmAlloc(uint256 farmId, uint256 allocPoint, bool withUpdate) public onlyOwner {
        if (withUpdate) updateAllFarms();

        totalAllocPoint = totalAllocPoint - farms[farmId].allocPoint + allocPoint;
        farms[farmId].allocPoint = allocPoint;
    }

    // View function to see deposited LP for a user.
    function getDeposited(uint256 farmId, address userAddress) public view returns (uint256) {
        User storage user = users[farmId][userAddress];

        return user.amount;
    }

    // View function to see pending ERC20s for a user.
    function getPendingReward(uint256 farmId, address userAddress) public view returns (uint256) {
        Farm storage farm = farms[farmId];
        User storage user = users[farmId][userAddress];
        uint256 accERC20PerShare = farm.accERC20PerShare;
        uint256 supply = farm.depositToken.balanceOf(address(this));

        if (block.number > farm.lastRewardBlock && supply != 0) {
            uint256 lastBlock = block.number < farm.blockTo ? block.number : farm.blockTo;
            uint256 nrOfBlocks = lastBlock - farm.lastRewardBlock;
            uint256 erc20Reward = nrOfBlocks * farm.rewardPerBlock; // * farm.allocPoint; // / totalAllocPoint;

            accERC20PerShare += erc20Reward * 1e36 / supply;
        }

        return (user.amount * accERC20PerShare / 1e36) - user.rewardDebt;
    }

    // View function for total reward the farm has yet to pay out.
    function getTotalPending(uint256 farmId) external view returns (uint256) {
        Farm storage farm = farms[farmId];

        if (block.number <= farm.blockFrom) return 0;

        uint256 lastBlock = block.number < farm.blockTo ? block.number : farm.blockTo;

        return farm.rewardPerBlock * (lastBlock - farm.blockFrom) - farm.paidOut;
    }

    // Update reward variables for all farms. Be careful of gas spending!
    function updateAllFarms() public {
        uint256 length = farms.length;

        for (uint256 farmId = 0; farmId < length; ++farmId) {
            updateFarm(farmId);
        }
    }

    // Update reward variables of the given farm to be up-to-date.
    function updateFarm(uint256 farmId) public {
        Farm storage farm = farms[farmId];
        uint256 lastBlock = block.number < farm.blockTo ? block.number : farm.blockTo;

        if (lastBlock <= farm.lastRewardBlock) return;

        uint256 supply = farm.depositToken.balanceOf(address(this));
        if (supply == 0) {
            farm.lastRewardBlock = lastBlock;
            return;
        }

        uint256 nrOfBlocks = lastBlock - farm.lastRewardBlock;
        uint256 erc20Reward = nrOfBlocks * farm.rewardPerBlock; // * farm.allocPoint; // / totalAllocPoint;

        farm.accERC20PerShare += erc20Reward * 1e36 / supply;
        farm.lastRewardBlock = block.number;
    }

    // Deposit LP tokens to Farm for ERC20 allocation.
    function deposit(uint256 farmId, uint256 value) public {
        Farm storage farm = farms[farmId];
        User storage user = users[farmId][msg.sender];
        updateFarm(farmId);

        if (user.amount > 0) {
            uint256 pendingAmount = (user.amount * farm.accERC20PerShare / 1e36) - user.rewardDebt;
            erc20Transfer(farmId, msg.sender, pendingAmount);
        }

        farm.depositToken.transferFrom(address(msg.sender), address(this), value);
        user.amount = user.amount + value;
        user.rewardDebt = user.amount * farm.accERC20PerShare / 1e36;
        user.timestamp = block.timestamp;

        emit Deposit(msg.sender, farmId, value);
    }

    // Withdraw LP tokens from Farm.
    function withdraw(uint256 farmId, uint256 value) public {
        Farm storage farm = farms[farmId];
        User storage user = users[farmId][msg.sender];

        require(user.timestamp + farm.lockTime <= block.timestamp, "WAIT_FOR_UNLOCK");
        require(user.amount >= value, "INVALID_VALUE");

//        if (value > user.amount) value = user.amount;

        updateFarm(farmId);
        uint256 pendingAmount = (user.amount * farm.accERC20PerShare / 1e36) - user.rewardDebt;
        erc20Transfer(farmId, msg.sender, pendingAmount);
        user.amount = user.amount - value;
        user.rewardDebt = user.amount * farm.accERC20PerShare / 1e36;
        user.timestamp = block.timestamp;
        farm.depositToken.transfer(address(msg.sender), value);

        emit Withdraw(msg.sender, farmId, value);
    }

    function claimReward(uint256 farmId) public {
        Farm storage farm = farms[farmId];
        User storage user = users[farmId][msg.sender];

        updateFarm(farmId);
        uint256 pendingAmount = (user.amount * farm.accERC20PerShare / 1e36) - user.rewardDebt;
        erc20Transfer(farmId, msg.sender, pendingAmount);
        user.rewardDebt = user.amount * farm.accERC20PerShare / 1e36;
        user.timestamp = block.timestamp;
    }

    // Transfer ERC20 and update the required ERC20 to payout all rewards
    function erc20Transfer(uint256 farmId, address to, uint256 value) internal {
        Farm storage farm = farms[farmId];

        rewardToken.transfer(to, value);
        farm.paidOut += value;
    }

    function getFarmMeta(uint256 farmId) public view returns (FarmMeta memory) {
        FarmMeta memory farmMeta;
        Farm storage farm = farms[farmId];
        User storage user = users[farmId][msg.sender];

        farmMeta.farm = farm;
        farmMeta.farmId = farmId;
        farmMeta.deposited = getDeposited(farmId, msg.sender);
        farmMeta.progress = (block.number - farm.blockFrom) * 100 / (farm.blockTo - farm.blockFrom);
        farmMeta.balance = farm.depositToken.balanceOf(msg.sender);
        farmMeta.tokenName = farm.depositToken.name();
        farmMeta.tokenSymbol = farm.depositToken.symbol();
        farmMeta.tokenDecimals = farm.depositToken.decimals();
        farmMeta.tokenAddress = address(farm.depositToken);
        farmMeta.supply = farm.depositToken.balanceOf(address(this));
        farmMeta.remainReward = getFarmRemainReward(farmId);

        if (user.timestamp > 0) {
            farmMeta.unlockTime = user.timestamp + farm.lockTime;
            farmMeta.pendingReward = getPendingReward(farmId, msg.sender);
        }

        return farmMeta;
    }

    function getAllFarmsMeta() public view returns (FarmMeta[] memory) {
        FarmMeta[] memory result = new FarmMeta[](farms.length);

        for (uint256 i = 0; i < farms.length; i++) {
            result[i] = getFarmMeta(i);
        }

        return result;
    }

    function getFarmRemainReward(uint256 farmId) public view returns (uint256) {
        Farm storage farm = farms[farmId];
        uint256 nBlocks = farm.blockTo - block.number;

        return farm.rewardPerBlock * nBlocks;
    }

    function getFarmMetaInfo() public view returns (FarmMetaInfo memory) {
        FarmMetaInfo memory metaInfo;

        metaInfo.farmCount = farms.length;

        for (uint256 i = 0; i < metaInfo.farmCount - 1; i++) {
            FarmMeta memory farmMeta = getFarmMeta(i);
            uint256 nBlocks = block.number - farmMeta.farm.blockFrom;

            metaInfo.totalSupply += farmMeta.farm.allocPoint;
            metaInfo.rewardPerBlock += farmMeta.farm.rewardPerBlock;
            metaInfo.currentSupply += nBlocks * metaInfo.rewardPerBlock;
        }

        return metaInfo;
    }
}
