// SPDX-License-Identifier: MIT
pragma solidity ^0.8.0;

import "@openzeppelin/contracts-upgradeable/token/ERC20/ERC20Upgradeable.sol";
import "@openzeppelin/contracts-upgradeable/token/ERC20/extensions/ERC20BurnableUpgradeable.sol";
import "@openzeppelin/contracts-upgradeable/access/OwnableUpgradeable.sol";

// npx hardhat run --network testnet scripts/deploy_token.js
// npx hardhat console --network testnet

// const SBRToken = await ethers.getContractFactory("SBRToken"); // dev
// const token = await SBRToken.attach("0xAe5057BB185fC820219e21bC7382c0DE7A42fE86");
// await token.balanceOf("0x6E62eFEAb443bd1B233C4DF795Da4794511a8907")
// await token.approve("0x6E62eFEAb443bd1B233C4DF795Da4794511a8907", ethers.BigNumber.from(100).mul(ethers.BigNumber.from(10).pow(18)).toString())
// await token.mint("0x90F8bf6A479f320ead074411a4B0e7944Ea8c9C1", ethers.BigNumber.from(750).mul(ethers.BigNumber.from(10).pow(18)).toString())

// testnet
// const token = await (await ethers.getContractFactory("SBRToken")).attach("0x8F5393F5c4727DBa352cab9B794b00E8e44A6F98");
// await token.approve("0xDd36B882b63e81E5DCc91eFE8D6bF654b695f933", ethers.BigNumber.from(250).mul(ethers.BigNumber.from(10).pow(18)).toString())
// await token.mint("0x7a964F429954de9772F7179390416098903eC8A0", ethers.BigNumber.from(750).mul(ethers.BigNumber.from(10).pow(18)).toString())

// const token = await (await ethers.getContractFactory("SBRToken")).attach("0x8F5393F5c4727DBa352cab9B794b00E8e44A6F98");
// await token.approve("0x977D9486888dE0Fe055bdbF404CbCd6a20a30585", ethers.BigNumber.from(25000000).mul(ethers.BigNumber.from(10).pow(18)).toString())
// await token.mint("0x7a964F429954de9772F7179390416098903eC8A0", ethers.BigNumber.from(10000).mul(ethers.BigNumber.from(10).pow(18)).toString())
//
// const busdtoken = await (await ethers.getContractFactory("SBRToken")).attach("0xed24fc36d5ee211ea25a80239fb8c4cfd80f12ee");
// await busdtoken.approve("0x977D9486888dE0Fe055bdbF404CbCd6a20a30585", ethers.BigNumber.from(25000000).mul(ethers.BigNumber.from(10).pow(18)).toString())


contract SBRToken is ERC20Upgradeable, ERC20BurnableUpgradeable, OwnableUpgradeable {
    function initialize() initializer public {
        __ERC20_init("SweetBerry Token", "SBR");
        __ERC20Burnable_init();
        __Ownable_init();
    }

    function mint(address to, uint256 amount) public onlyOwner {
        _mint(to, amount);
    }
}
