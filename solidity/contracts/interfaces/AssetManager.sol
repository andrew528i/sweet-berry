// SPDX-License-Identifier: MIT
pragma solidity ^0.8.0;

import "@openzeppelin/contracts/token/ERC20/ERC20.sol";
import "@chainlink/contracts/src/v0.8/interfaces/AggregatorV3Interface.sol";

interface IAssetManagerTypes {
    enum AssetType {Token, Native, Stock, Fiat}

    enum Blockchain {SmartChain, Ethereum, Bitcoin, Tron}

    struct Asset {
        string symbol;
        uint16 decimals;
        AssetType type_;
        ERC20 token;
        Blockchain blockchain;
    }

    enum AssetPairState { Inactive, Active, TemporarySuspended, Suspended }

    struct AssetPair {
        Asset first;
        Asset second;
        AggregatorV3Interface priceFeed;
        AssetPairState state;
        uint256 createdAt;
        uint256 roundDuration;
        uint256 maxBet;
    }
}

interface IAssetManager is IAssetManagerTypes {
    function addTokenAsset(address tokenAddress) external;
    function addNativeAsset(string memory symbol, uint16 decimals, uint blockchain) external;
    function addStockAsset(string memory symbol) external;
    function addFiatAsset(string memory symbol) external;
    function getAsset(uint256 index) external view returns (Asset memory);
    function getAssetCount() external view returns (uint256);
    function getAssetBySymbol(string memory symbol) external view returns (Asset memory);

    function addAssetPair(
        string memory firstSymbol,
        string memory secondSymbol,
        address priceFeedAddress,
        uint256 state,
        uint256 createdAt,
        uint256 roundDuration
    ) external;
    function getAssetPair(uint256 index) external view returns (AssetPair memory);
    function getAssetPairCount() external view returns (uint256);
}
