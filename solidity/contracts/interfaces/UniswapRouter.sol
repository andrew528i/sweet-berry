// SPDX-License-Identifier: MIT
pragma solidity ^0.8.0;

interface UniswapRouter {
    function getAmountsOut(uint amountIn, address[] memory path) external view returns (uint[] memory amounts);
}
