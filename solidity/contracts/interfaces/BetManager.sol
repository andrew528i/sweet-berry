// SPDX-License-Identifier: MIT
pragma solidity ^0.8.0;

import "./AssetManager.sol";

interface IBetManagerTypes is IAssetManagerTypes {
    enum BetDirection {Up, Down}

    struct Round {
        uint80 linkRoundIdFrom;
        uint256 linkTimestampFrom;
        int256 linkPriceFrom;

        uint80 linkRoundIdTo;
        uint256 linkTimestampTo;
        int256 linkPriceTo;

        uint256 upBetsValue;
        uint256 upBetsCount;
        uint256 downBetsValue;
        uint256 downBetsCount;

        uint256 paidValue;
    }

    struct Bet {
        uint256 value;
        BetDirection direction;
        uint256 createdAt;
        uint256 rewardValue;
    }

    struct RoundMeta {
        Round round;
        AssetPair assetPair;
        uint256 progress;
        uint256 roundId;
        int256 price;
        uint8 decimals;
        PriceTick[] history;
        uint256 upCoef;
        uint256 downCoef;
        uint256 pendingReward;
        uint256 timestampFrom;
        uint256 timestampTo;
        int256 priceFrom;
        int256 priceTo;
    }

    struct PriceTick {
        uint256 timestamp;
        int256 price;
    }

    struct PriceMetaInfo {
        uint256 upCount;
        uint256 downCount;
        uint256 volatileCount;
        uint256 highCoefCount;
        uint256 currentRoundValue;
        uint256 pairsCount;
        uint256 tokenPrice;
    }
}
