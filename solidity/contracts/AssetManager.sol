// SPDX-License-Identifier: MIT
pragma solidity ^0.8.0;

import "@openzeppelin/contracts-upgradeable/access/OwnableUpgradeable.sol";
import "@openzeppelin/contracts/token/ERC20/ERC20.sol";

import "./util/String.sol";
import "./interfaces/AssetManager.sol";

// npx hardhat run --network dev scripts/deploy_asset_pair.js
// npx hardhat console --network dev

// const c = (await ethers.getContractFactory("AssetManager")).attach("0x43192D86e8452957D8076908Dc04ddb6a6B63a61");
// await c.getAssetPairBySymbols("BTC", "USD")

// const c = (await ethers.getContractFactory("AssetManager")).attach("0x5516aAc9b95F21c1338Fa2EB8463cA2E4C5013c2");
// await c.getAssetPairBySymbols("BTC", "USD")
// await c.addAssetPair("ADA", "USD", "0x5e66a1775BbC249b5D51C13d29245522582E671C", 1, 1623422720, 3600 * 6)

contract AssetManager is OwnableUpgradeable, IAssetManager {
    using String for string;

    Asset[] private assets;
    AssetPair[] private assetPairs;

    modifier onlyNewAsset(Asset memory asset) {
        for (uint i = 0; i < assets.length; i++) {
            require(asset.symbol.notEquals(assets[i].symbol), "ASSET_ALREADY_EXISTS");
        }

        _;
    }

    function initialize() initializer public {
        __Ownable_init();

//        addTokenAsset(0xAe5057BB185fC820219e21bC7382c0DE7A42fE86);
        addTokenAsset(0x8F5393F5c4727DBa352cab9B794b00E8e44A6F98);
        addTokenAsset(0xF9d908355D70f93BBBCCE9Fe08e0d55AAB3CB2e6);
        addTokenAsset(0x8FDA19155dFaC64E7359595a2b4ed2F05A1E4391);
        addTokenAsset(0xCF51b699Fa7797cE305B60e3F71092daa80c4230);
        addNativeAsset("BNB", 18, uint256(Blockchain.SmartChain));
        addNativeAsset("ETH", 18, uint256(Blockchain.Ethereum));
        addNativeAsset("BTC", 8, uint256(Blockchain.Bitcoin));
        addStockAsset("AAPL");
        addFiatAsset("USD");
        addFiatAsset("EUR");

        addAssetPair(
            "BTC", "USD", 0x5741306c21795FdCBb9b265Ea0255F499DFe515C,
            uint256(AssetPairState.Active), block.timestamp, 6 hours);
        addAssetPair(
            "ETH", "USD", 0x143db3CEEfbdfe5631aDD3E50f7614B6ba708BA7,
            uint256(AssetPairState.Active), block.timestamp, 4 hours);
        addAssetPair(
            "LINK", "USD", 0x1B329402Cb1825C6F30A0d92aB9E2862BE47333f,
            uint256(AssetPairState.Active), block.timestamp, 3 hours);
        addAssetPair(
            "BNB", "USD", 0x2514895c72f50D8bd4B4F9b1110F0D6bD2c97526,
            uint256(AssetPairState.Active), block.timestamp, 8 hours);
    }

    // Asset
    function addAsset(Asset memory asset) private onlyOwner onlyNewAsset(asset) {
        assets.push(asset);
    }

    function addTokenAsset(address tokenAddress) public override {
        ERC20 token = ERC20(tokenAddress);
        string memory symbol = token.symbol();
        uint16 decimals = token.decimals();

        Asset memory newAsset = Asset(symbol, decimals, AssetType.Token, token, Blockchain.SmartChain);
        addAsset(newAsset);
    }

    function addNativeAsset(string memory symbol, uint16 decimals, uint blockchain) public override {
        Asset memory newAsset = Asset(symbol, decimals, AssetType.Native, ERC20(0x0000000000000000000000000000000000000000), Blockchain(blockchain));
        addAsset(newAsset);
    }

    function addStockAsset(string memory symbol) public override {
        Asset memory newAsset = Asset(symbol, 0, AssetType.Stock, ERC20(0x0000000000000000000000000000000000000000), Blockchain(0));
        addAsset(newAsset);
    }

    function addFiatAsset(string memory symbol) public override {
        Asset memory newAsset = Asset(symbol, 2, AssetType.Fiat, ERC20(0x0000000000000000000000000000000000000000), Blockchain(0));
        addAsset(newAsset);
    }

    function getAsset(uint256 index) public view override returns (Asset memory) {
        require(index < assets.length, "ASSET_NOT_FOUND");

        return assets[index];
    }

    function getAssetCount() public view override returns (uint256) {
        return assets.length;
    }

    function getAssetBySymbol(string memory symbol) public view override returns (Asset memory) {
        for (uint i = 0; i < assets.length; i++) {
            if (symbol.equals(assets[i].symbol)) return assets[i];
        }

        revert("ASSET_NOT_FOUND");
    }

    // Asset Pair
    function addAssetPair(
        string memory firstSymbol,
        string memory secondSymbol,
        address priceFeedAddress,
        uint256 state,
        uint256 createdAt,
        uint256 roundDuration
    ) public override onlyOwner {
        require(firstSymbol.notEquals(secondSymbol), "ASSET_PAIR_SAME_SYMBOLS");

        Asset memory firstAsset = getAssetBySymbol(firstSymbol);
        Asset memory secondAsset = getAssetBySymbol(secondSymbol);
        AssetPair memory assetPair = AssetPair(
            firstAsset, secondAsset, AggregatorV3Interface(priceFeedAddress),
            AssetPairState(state), createdAt, roundDuration, 10 ether);

        assetPairs.push(assetPair);
    }

    function getAssetPair(uint256 index) public view override returns (AssetPair memory) {
        require(index < assetPairs.length, "ASSET_PAIR_NOT_FOUND");

        return assetPairs[index];
    }

    function getAssetPairCount() public view override returns (uint256) {
        return assetPairs.length;
    }

    function getAllPairs() public view returns (AssetPair[] memory) {
        return assetPairs;
    }
}
