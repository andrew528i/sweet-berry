// SPDX-License-Identifier: MIT
pragma solidity ^0.8.0;

import "@openzeppelin/contracts-upgradeable/access/OwnableUpgradeable.sol";
import "@openzeppelin/contracts/token/ERC20/ERC20.sol";

import "./interfaces/AssetManager.sol";
import "./interfaces/BetManager.sol";
import "./interfaces/UniswapRouter.sol";

// npx hardhat run --network dev scripts/deploy_bet.js
// npx hardhat console --network dev

// const c = (await ethers.getContractFactory("BetManager")).attach("0x6E62eFEAb443bd1B233C4DF795Da4794511a8907");
// const betValue = ethers.BigNumber.from(1).mul(ethers.BigNumber.from(10).pow(18)).toString();
// await c.makeBet(0, 0, 1, betValue, {gasPrice: ethers.utils.parseUnits('10', 'gwei'), gasLimit: 1000000});

// const c = (await ethers.getContractFactory("BetManager")).attach("0xDd36B882b63e81E5DCc91eFE8D6bF654b695f933");
// const betValue = ethers.BigNumber.from(3).mul(ethers.BigNumber.from(10).pow(18)).toString();
// await c.makeBet(1, 4, 1, betValue, {gasPrice: ethers.utils.parseUnits('10', 'gwei'), gasLimit: 1000000});

contract BetManager is OwnableUpgradeable, IBetManagerTypes {
    IAssetManager private assetManager;
    mapping(uint256 => mapping(uint256 => mapping(address => Bet))) private bets;
    mapping(uint256 => mapping(uint256 => Round)) private rounds;

    address private feeAddress;
    uint256 private feeAmount;

    event BetCreated(uint256 assetPairId, uint256 roundId, BetDirection direction, uint256 value);
    event RewardCollected(uint256 assetPairId, uint256 roundId, uint256 rewardValue);

    function initialize() initializer public {
        __Ownable_init();

        assetManager = IAssetManager(0x5516aAc9b95F21c1338Fa2EB8463cA2E4C5013c2);
        feeAddress = 0x7a964F429954de9772F7179390416098903eC8A0;
        feeAmount = 50;
    }

    function setFeeData(address _feeAddress, uint256 _feeAmount) public onlyOwner {
        feeAddress = _feeAddress;
        feeAmount = _feeAmount;
    }

    function getLinkRoundDataByTimestamp(
        uint256 timestamp,
        AssetPair memory assetPair
    ) private view returns (uint80, int256, uint256, uint256, uint80) {
        (
            uint80 linkRoundId, int256 linkPrice, uint256 linkTimestamp,
            uint256 linkUpdatedAt, uint80 linkAnsweredInRound
        ) = assetPair.priceFeed.latestRoundData();
        uint256 limit = 75;

        for (uint i = 1; i < limit; i++) {
            if (linkTimestamp < timestamp) return (
                linkRoundId, linkPrice, linkTimestamp,
                linkUpdatedAt, linkAnsweredInRound);

            linkRoundId--;

            try assetPair.priceFeed.getRoundData(linkRoundId) returns (
                uint80, int256 linkPriceNew, uint256 linkTimestampNew,
                uint256 linkUpdatedAtNew, uint80 linkAnsweredInRoundNew
            ) {
                linkPrice = linkPriceNew;
                linkTimestamp = linkTimestampNew;
                linkUpdatedAt = linkUpdatedAtNew;
                linkAnsweredInRound = linkAnsweredInRoundNew;
            } catch (bytes memory) {}
        }

        return (0, 0, 0, 0, 0);
//        revert("LINK_ROUND_ID_NOT_FOUND");
    }

    function roundIsOpened(uint256 assetPairId, uint256 roundId) private view returns (bool) {
        return rounds[assetPairId][roundId].linkTimestampFrom != 0;
    }

    function openRound(uint256 assetPairId, uint256 roundId) private {
        require(!roundIsOpened(assetPairId, roundId), "ROUND_ALREADY_OPENED");

        AssetPair memory assetPair = assetManager.getAssetPair(assetPairId);
        uint256 roundTimestampFrom = assetPair.createdAt + roundId * assetPair.roundDuration;

        (
            uint80 linkRoundId, int256 linkPrice, uint256 linkTimestamp, ,
        ) = getLinkRoundDataByTimestamp(roundTimestampFrom, assetPair);

        rounds[assetPairId][roundId].linkRoundIdFrom = linkRoundId;
        rounds[assetPairId][roundId].linkPriceFrom = linkPrice;
        rounds[assetPairId][roundId].linkTimestampFrom = linkTimestamp;
    }

    function roundIsClosed(uint256 assetPairId, uint256 roundId) private view returns (bool) {
        return rounds[assetPairId][roundId].linkTimestampTo != 0;
    }

    function closeRound(uint256 assetPairId, uint256 roundId) private {
        AssetPair memory assetPair = assetManager.getAssetPair(assetPairId);
        uint256 roundTimestampTo = assetPair.createdAt + (roundId + 1) * assetPair.roundDuration;

        require(block.timestamp >= roundTimestampTo, "TOO_EARLY_TO_CLOSE_ROUND");
        require(!roundIsClosed(assetPairId, roundId), "ROUND_ALREADY_CLOSED");
        require(roundIsOpened(assetPairId, roundId), "ROUND_NOT_OPENED");

        (
            uint80 linkRoundId, int256 linkPrice, uint256 linkTimestamp, ,
        ) = getLinkRoundDataByTimestamp(roundTimestampTo, assetPair);

        rounds[assetPairId][roundId].linkRoundIdTo = linkRoundId;
        rounds[assetPairId][roundId].linkPriceTo = linkPrice;
        rounds[assetPairId][roundId].linkTimestampTo = linkTimestamp;
    }

    function getCurrentRoundId(uint256 assetPairId) public view returns (uint256) {
        AssetPair memory assetPair = assetManager.getAssetPair(assetPairId);

        return (block.timestamp - assetPair.createdAt) / assetPair.roundDuration;
    }

    function getRound(uint256 assetPairId, uint256 roundId) public view returns (Round memory) {
        return rounds[assetPairId][roundId];
    }

    function getRoundProgress(uint256 assetPairId, uint256 roundId) public view returns (uint256) {
        AssetPair memory assetPair = assetManager.getAssetPair(assetPairId);
        uint256 roundTimestampFrom = assetPair.createdAt + roundId * assetPair.roundDuration;

        uint256 progress = ((block.timestamp - roundTimestampFrom) * 100) / assetPair.roundDuration;

        if (progress > 100) progress = 100;
        if (progress < 0) progress = 0;

        return progress;
    }

    function getRoundMeta(uint256 assetPairId, uint256 roundId) public view returns (RoundMeta memory) {
        AssetPair memory assetPair = assetManager.getAssetPair(assetPairId);
        RoundMeta memory roundMeta;
        Round memory round = getRound(assetPairId, roundId);

        roundMeta.round = round;
        (roundMeta.upCoef, roundMeta.downCoef) = getRoundCoefs(assetPairId, roundId);
        roundMeta.assetPair = assetPair;
        roundMeta.roundId = roundId;
        roundMeta.progress = getRoundProgress(assetPairId, roundId);

        (uint80 latestRoundId, int256 linkPrice, uint256 linkTimestamp, ,) = assetPair.priceFeed.latestRoundData();

        roundMeta.price = linkPrice;
        roundMeta.decimals = assetPair.priceFeed.decimals();
        uint80 roundDelta = 41;
        roundMeta.history = new PriceTick[](roundDelta + 1);

        for (uint80 i = roundDelta; i != 0; i--) {
            try assetPair.priceFeed.getRoundData(latestRoundId - i) returns (
                uint80, int256 linkPriceNew, uint256 linkTimestampNew, uint256, uint80
            ) {
                roundMeta.history[roundDelta - i] = PriceTick(linkTimestampNew, linkPriceNew);
            } catch (bytes memory) {}

//            roundMeta.history[roundDelta - i] = PriceTick(1600000000 + i * 86400, 1005);
        }

        roundMeta.history[roundDelta] = PriceTick(linkTimestamp, linkPrice);
        roundMeta.pendingReward = pendingRoundReward(assetPairId, roundId);
        roundMeta.timestampFrom = assetPair.createdAt + roundId * assetPair.roundDuration;
        roundMeta.timestampTo = roundMeta.timestampFrom + assetPair.roundDuration;

        if (round.linkPriceFrom > 0) {
            roundMeta.priceFrom = round.linkPriceFrom;
        } else {
            (, roundMeta.priceFrom, , ,) = getLinkRoundDataByTimestamp(roundMeta.timestampFrom, assetPair);
        }

        if (round.linkPriceTo > 0) {
            roundMeta.priceTo = round.linkPriceTo;
        } else {
            (, roundMeta.priceTo, , ,) = getLinkRoundDataByTimestamp(roundMeta.timestampTo, assetPair);
        }

        return roundMeta;
    }

    function getCurrentRoundMeta(uint256 assetPairId) public view returns (RoundMeta memory) {
        uint256 roundId = getCurrentRoundId(assetPairId);

        return getRoundMeta(assetPairId, roundId);
    }

    function getRoundMetaHistory(uint256 assetPairId, uint256 roundCount) public view returns (RoundMeta[] memory) {
        uint256 roundId = getCurrentRoundId(assetPairId);

        RoundMeta[] memory roundsMeta = new RoundMeta[](roundCount);

        for (uint256 i = 1; i <= roundCount; i++) {
            if (roundId - i < 0) {
                break;
            }

            roundsMeta[i - 1] = getRoundMeta(assetPairId, roundId - i);
        }

//        roundsMeta[0] = getRoundMeta(assetPairId, roundId - 1);

        return roundsMeta;
    }

    function makeBet(uint256 assetPairId, uint256 roundId, uint8 direction, uint256 value) public {
        uint currentRoundId = getCurrentRoundId(assetPairId);
        ERC20 token = assetManager.getAsset(0).token;

        require(getRoundProgress(assetPairId, roundId) < 80, "ROUND_DEADLINE");
        require(!roundIsClosed(assetPairId, roundId), "ROUND_CLOSED");
        require(bets[assetPairId][roundId][msg.sender].value == 0, "BET_EXISTS");
        require(roundId == currentRoundId || roundId == currentRoundId + 1, "WRONG_ROUND_ID");
        require(token.allowance(msg.sender, address(this)) >= value, "APPROVE_FIRST");

        if (!roundIsOpened(assetPairId, roundId)) openRound(assetPairId, roundId);
        if (roundId > 0 && !roundIsClosed(assetPairId, roundId - 1) && roundIsOpened(assetPairId, roundId - 1)) {
            closeRound(assetPairId, roundId - 1);
        }

        uint256 taxValue = value * feeAmount / 10000;
        uint256 betValue = value - taxValue;

        token.transferFrom(msg.sender, address(this), betValue);
        token.transferFrom(msg.sender, feeAddress, taxValue);
        BetDirection betDirection = BetDirection(direction);

        if (betDirection == BetDirection.Up) {
            rounds[assetPairId][roundId].upBetsCount++;
            rounds[assetPairId][roundId].upBetsValue += value;
        } else if (betDirection == BetDirection.Down) {
            rounds[assetPairId][roundId].downBetsCount++;
            rounds[assetPairId][roundId].downBetsValue += value;
        } else revert("UNKNOWN_DIRECTION");

        bets[assetPairId][roundId][msg.sender] = Bet(value, betDirection, block.timestamp, 0);

        emit BetCreated(assetPairId, roundId, betDirection, value);
    }

    function getBet(uint256 assetPairId, uint256 roundId) public view returns (Bet memory) {
        return bets[assetPairId][roundId][msg.sender];
    }

    function getRoundCoefs(uint256 assetPairId, uint256 roundId) public view returns (uint256, uint256) {
        uint256 upValue = rounds[assetPairId][roundId].upBetsValue;
        uint256 downValue = rounds[assetPairId][roundId].downBetsValue;
        uint256 total = upValue + downValue;
        uint256 upCoef = 100;
        uint256 downCoef = 100;

        if (upValue != 0) upCoef = total * 100 / upValue;
        if (downValue != 0) downCoef = total * 100 / downValue;

        return (upCoef, downCoef);
    }

    function pendingRoundReward(uint256 assetPairId, uint256 roundId) public view returns (uint256) {
//        require(roundIsOpened(assetPairId, roundId), "ROUND_NOT_OPENED");
//        require(getRoundProgress(assetPairId, roundId) == 100, "ROUND_NOT_ENDED");

//        if (roundIsOpened(assetPairId, roundId)) return 0;
        if (getRoundProgress(assetPairId, roundId) != 100) return 0;
        if (bets[assetPairId][roundId][msg.sender].rewardValue != 0) return 0;

        Round memory round = getRound(assetPairId, roundId);

        if (!roundIsClosed(assetPairId, roundId)) {
            AssetPair memory assetPair = assetManager.getAssetPair(assetPairId);
            uint256 roundTimestampTo = assetPair.createdAt + (roundId + 1) * assetPair.roundDuration;
            (, round.linkPriceTo, , ,) = getLinkRoundDataByTimestamp(roundTimestampTo, assetPair);
        }

        BetDirection betDirection = bets[assetPairId][roundId][msg.sender].direction;
        (uint256 upCoef, uint256 downCoef) = getRoundCoefs(assetPairId, roundId);
        uint256 betsCount = round.upBetsCount + round.downBetsCount;

        if (betDirection == BetDirection.Up && round.linkPriceFrom < round.linkPriceTo) {
            if (betsCount == 1) return bets[assetPairId][roundId][msg.sender].value;

            return bets[assetPairId][roundId][msg.sender].value * upCoef / 100;
        }

        if (betDirection == BetDirection.Down && round.linkPriceFrom >= round.linkPriceTo) {
            if (betsCount == 1) return bets[assetPairId][roundId][msg.sender].value;

            return bets[assetPairId][roundId][msg.sender].value * downCoef / 100;
        }

        return 0;
    }

    function collectRoundReward(uint256 assetPairId, uint256 roundId) public {
        uint256 rewardValue = pendingRoundReward(assetPairId, roundId);

        if (!roundIsClosed(assetPairId, roundId)) closeRound(assetPairId, roundId);

        require(bets[assetPairId][roundId][msg.sender].rewardValue == 0, "ALREADY_PAID");
        require(rewardValue > 0, "NO_REWARD");

        ERC20 token = assetManager.getAsset(0).token;
        token.transfer(msg.sender, rewardValue);
        bets[assetPairId][roundId][msg.sender].rewardValue = rewardValue;
        rounds[assetPairId][roundId].paidValue += rewardValue;

        emit RewardCollected(assetPairId, roundId, rewardValue);
    }

    function getTokenPrice() public view returns (uint) {
        UniswapRouter router = UniswapRouter(0x9Ac64Cc6e4415144C455BD8E4837Fea55603e5c3);
        address[] memory path = new address[](4);

        path[0] = address(assetManager.getAsset(0).token); // SBR
        path[1] = address(0xae13d989daC2f0dEbFf460aC112a837C89BAa7cd); // WBNB
        path[2] = address(0x8BaBbB98678facC7342735486C851ABD7A0d17Ca); // ETH
        path[3] = address(0x78867BbEeF44f2326bF8DDd1941a4439382EF2A7); // BUSD

        uint amount = 1 ether;

        return router.getAmountsOut(amount, path)[3];
    }

    function getMetaInfo() public view returns (PriceMetaInfo memory) {
        PriceMetaInfo memory metaInfo;

        metaInfo.pairsCount = assetManager.getAssetPairCount();

        for (uint256 i = 0; i < metaInfo.pairsCount; ++i) {
            RoundMeta memory roundMeta = getCurrentRoundMeta(i);
            int256 priceFrom = roundMeta.history[0].price;
            int256 priceTo = roundMeta.history[roundMeta.history.length - 1].price;

            if (priceFrom < priceTo) {
                metaInfo.upCount++;
            } else metaInfo.downCount++;

            (uint256 upCoef, uint256 downCoef) = getRoundCoefs(i, roundMeta.roundId);
            int256 change = 0;

            if (priceFrom > 0) {
                change = (priceTo - priceFrom) * 100 / priceFrom;
            }

            if (change > 4) metaInfo.volatileCount++;
            if (upCoef > 250 || downCoef > 250) metaInfo.highCoefCount++;

            metaInfo.currentRoundValue += roundMeta.round.upBetsValue + roundMeta.round.downBetsValue;
        }

        metaInfo.tokenPrice = getTokenPrice();

        return metaInfo;
    }
}
