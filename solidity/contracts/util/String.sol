// SPDX-License-Identifier: MIT
pragma solidity ^0.8.0;

library String {
    function equals(string memory self, string memory other) internal pure returns (bool) {
        bytes memory selfBytes = bytes(self);
        bytes memory otherBytes = bytes(other);

        if (selfBytes.length != otherBytes.length) return false;

        for (uint i = 0; i < selfBytes.length; i ++) {
            if (selfBytes[i] != otherBytes[i]) return false;
        }

        return true;
    }

    function notEquals(string memory self, string memory other) internal pure returns (bool) {
        return !equals(self, other);
    }
}
