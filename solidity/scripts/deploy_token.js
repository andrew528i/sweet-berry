const { ethers, upgrades } = require("hardhat");

async function main() {
  const SBRToken = await ethers.getContractFactory("SBRToken");
  const token = await upgrades.deployProxy(SBRToken);
  await token.deployed();

  console.log("SBRToken deployed to:", token.address);
}

main();
