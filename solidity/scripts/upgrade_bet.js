const { ethers, upgrades } = require("hardhat");

async function main() {
  const BetManager = await ethers.getContractFactory("BetManager");
  // const betManager = await upgrades.upgradeProxy("0x6E62eFEAb443bd1B233C4DF795Da4794511a8907", BetManager);
  const betManager = await upgrades.upgradeProxy("0xDd36B882b63e81E5DCc91eFE8D6bF654b695f933", BetManager);

  console.log("BetManager upgraded: ", betManager.address);
}

main();
