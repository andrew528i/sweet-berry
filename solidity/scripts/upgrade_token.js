const { ethers, upgrades } = require("hardhat");

async function main() {
  const SBRToken = await ethers.getContractFactory("SBRToken");
  // const token = await upgrades.upgradeProxy("0xAe5057BB185fC820219e21bC7382c0DE7A42fE86", SBRToken);
  const token = await upgrades.upgradeProxy("0x8F5393F5c4727DBa352cab9B794b00E8e44A6F98", SBRToken);

  console.log("SBRToken upgraded: ", token.address);
}

main();
