const { ethers, upgrades } = require("hardhat");

async function main() {
  const BetManager = await ethers.getContractFactory("BetManager");
  const betManager = await upgrades.deployProxy(BetManager);
  await betManager.deployed();

  console.log("BetManager deployed to:", betManager.address);
}

main();
