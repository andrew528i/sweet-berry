const { ethers, upgrades } = require("hardhat");

async function main() {
  const FarmManager = await ethers.getContractFactory("FarmManager");
  const farmManager = await upgrades.upgradeProxy("0x977D9486888dE0Fe055bdbF404CbCd6a20a30585", FarmManager);

  console.log("FarmManager upgraded: ", farmManager.address);
}

main();
