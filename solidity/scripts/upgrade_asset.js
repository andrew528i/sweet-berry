const { ethers, upgrades } = require("hardhat");

async function main() {
  const AssetManager = await ethers.getContractFactory("AssetManager");
  // const assetManager = await upgrades.upgradeProxy("0x43192D86e8452957D8076908Dc04ddb6a6B63a61", AssetManager);
  const assetManager = await upgrades.upgradeProxy("0x5516aAc9b95F21c1338Fa2EB8463cA2E4C5013c2", AssetManager);

  console.log("AssetManager upgraded: ", assetManager.address);
}

main();
