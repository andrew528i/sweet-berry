const { ethers, upgrades } = require("hardhat");

async function main() {
  const AssetManager = await ethers.getContractFactory("AssetManager");
  const assetManager = await upgrades.deployProxy(AssetManager);
  await assetManager.deployed();

  console.log("AssetManager deployed to:", assetManager.address);
}

main();
