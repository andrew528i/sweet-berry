const { ethers, upgrades } = require("hardhat");

async function main() {
  const FarmManager = await ethers.getContractFactory("FarmManager");
  const farmManager = await upgrades.deployProxy(FarmManager);
  await farmManager.deployed();

  console.log("FarmManager deployed to:", farmManager.address);
}

main();
