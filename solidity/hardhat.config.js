require('@openzeppelin/hardhat-upgrades');
require('ethers');

/**
 * @type import('hardhat/config').HardhatUserConfig
 */
module.exports = {
  solidity: "0.8.4",
  networks: {
    dev: {
      url: 'http://localhost:8545/',
      from: '0x90F8bf6A479f320ead074411a4B0e7944Ea8c9C1',
      accounts: ['4f3edf983ac636a65a842ce7c78d9aa706d3b113bce9c46f30d7d21715b23b1d'],
      gas: "auto",
      gasPrice: "auto",
      networkId: '*',
    },
    testnet: {
      url: 'https://data-seed-prebsc-1-s1.binance.org:8545',
      from: '0x7a964F429954de9772F7179390416098903eC8A0',
      accounts: ['12f03aac7da19cea19db188d258a8dbc9e8cc1368d4d6c94e1b6c068b48faf7d'],
      gas: "auto",
      gasPrice: "auto",
      networkId: '*',
    }
  }
};
