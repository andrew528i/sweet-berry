// testnet
export const config = {
    providerUrl: 'https://data-seed-prebsc-1-s2.binance.org:8545',
    explorerUrl: 'https://testnet.bscscan.com/',
    exchangeUrl: 'https://pancake.kiemtienonline360.com/#',
    network: {
        chainId: '0x61',
        chainName: 'Smart Chain Testnet',
        nativeCurrency: {
            name: 'Binance Coin',
            symbol: 'BNB',
            decimals: 18
        },
        rpcUrls: ['https://data-seed-prebsc-1-s1.binance.org:8545/'],
        blockExplorerUrls: ['https://testnet.bscscan.com']
    },
    token: {
        address: '0x8f5393f5c4727dba352cab9b794b00e8e44a6f98',
        decimals: 18,
        displayDecimals: 4,
        symbol: 'SBR',
        iconUrl: '@/assets/image/sbr.png',
    },
    assetManager: {
        address: '0x5516aAc9b95F21c1338Fa2EB8463cA2E4C5013c2',
    },
    betManager: {
        address: '0xdd36b882b63e81e5dcc91efe8d6bf654b695f933',
    },
    farmManager: {
        address: '0x977D9486888dE0Fe055bdbF404CbCd6a20a30585',
    },
    assetMeta: {
        USDT: {
            decimals: 18,
            displayDecimals: 2,
            iconUrl: 'https://exchange.pancakeswap.finance/images/coins/0x55d398326f99059ff775485246999027b3197955.png',
        },
        USDC: {
            decimals: 18,
            displayDecimals: 2,
            iconUrl: 'https://exchange.pancakeswap.finance/images/coins/0x8ac76a51cc950d9822d68b83fe1ad97b32cd580d.png',
        },
        BUSD: {
            decimals: 18,
            displayDecimals: 2,
            iconUrl: 'https://exchange.pancakeswap.finance/images/coins/0xe9e7cea3dedca5984780bafc599bd69add087d56.png',
        },
        ETH: {
            decimals: 18,
            displayDecimals: 4,
            iconUrl: 'https://exchange.pancakeswap.finance/images/coins/0x2170ed0880ac9a755fd29b2688956bd959f933f8.png',
        },
        BTC: {
            decimals: 8,
            displayDecimals: 6,
            iconUrl: 'https://exchange.pancakeswap.finance/images/coins/0x7130d2a12b9bcbfae4f2634d864a1ee1ce3ead9c.png',
        },
        BNB: {
            decimals: 18,
            displayDecimals: 4,
            iconUrl: 'https://exchange.pancakeswap.finance/images/coins/0xbb4cdb9cbd36b01bd1cbaebf2de08d9173bc095c.png',
        },
        ADA: {
            decimals: 18,
            displayDecimals: 4,
            iconUrl: 'https://exchange.pancakeswap.finance/images/coins/0x3ee2200efb3400fabb9aacf31297cbdd1d435d47.png',
        },
        DOGE: {
            decimals: 18,
            displayDecimals: 6,
            iconUrl: 'https://assets.trustwalletapp.com/blockchains/doge/info/logo.png',
        },
        TRX: {
            decimals: 18,
            displayDecimals: 4,
            iconUrl: 'https://assets.trustwalletapp.com/blockchains/tron/info/logo.png',
        },
        CAKE: {
            decimals: 18,
            displayDecimals: 2,
            iconUrl: 'https://exchange.pancakeswap.finance/images/coins/0x0e09fabb73bd3ade0a17ecc321fd13a19e81ce82.png',
        },
        LINK: {
            decimals: 8,
            displayDecimals: 2,
            iconUrl: 'https://exchange.pancakeswap.finance/images/coins/0xf8a0bf9cf54bb92f17374d9e9a321e6a111a51bd.png',
        },
        USD: {
            decimals: 2,
            displayDecimals: 2,
            iconUrl: 'https://assets.trustwalletapp.com/blockchains/ethereum/assets/0xdAC17F958D2ee523a2206206994597C13D831ec7/logo.png',
        },
        // eslint-disable-next-line
    } as Record<string, any>,
    events: {
        ERROR: 'error',
        OPEN_BET_MODAL: 'openBetModal',
        OPEN_WALLET_MODAL: 'openWalletModal',
        SYNC_BLOCKCHAIN: 'syncBlockchain',
        FARM_EXPANDED: 'farmExpanded',
        ASSET_PAIR_EXPANDED: 'assetPairExpanded',
    }
};
