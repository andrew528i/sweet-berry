export default {
    en: {
        connectWalletBtnText: 'Connect wallet',
        chooseWalletModalTitle: 'Choose wallet',
        close: 'Close',
        disconnect: 'Disconnect',
    },
    ru: {
        connectWalletBtnText: 'Подключить кошелек',
        chooseWalletModalTitle: 'Выберите кошелек',
        close: 'Закрыть',
        disconnect: 'Отключиться',
    },
}
