import {WalletProvider, WalletType} from '@/types/wallet'
import {ActionContext} from 'vuex'

interface WalletState {
    address: string | null
    provider: WalletProvider | null
    allProviders: WalletProvider[]
}

type Context = ActionContext<WalletState, WalletState>

export default {
    namespaced: true,

    state: (): WalletState => ({
        address: null,
        provider: null,
        allProviders: [{
            name: 'MetaMask',
            iconUrl: 'https://cdn.iconscout.com/icon/free/png-512/metamask-2728406-2261817.png',
            type: WalletType.MetaMask,
        }, {
            name: 'Binance Wallet',
            iconUrl: 'https://bsc.valuedefi.io/static/media/bsc-wallet.d777f0b4.svg',
            type: WalletType.Binance,
        }]
    }),

    getters: {
        isConnected(state: WalletState): boolean { return !!state.address && !!state.provider },
    },

    mutations: {
        setAddress(state: WalletState, address: string | null): void {
            state.address = address
        },
        setProvider(state: WalletState, provider: WalletProvider | null): void {
            state.provider = provider
        }
    },

    actions: {
        setAddress ({ commit }: Context, address: string): void {
            commit('setAddress', address)
        },
        setProvider({ commit }: Context, provider: WalletProvider): void {
            commit('setProvider', provider)
        },
        disconnect({ commit }: Context): void {
            commit('setAddress', null)
            commit('setProvider', null)
        }
    }
}
