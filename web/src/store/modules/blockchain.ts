import {Asset, AssetPair} from "@/types/blockchain";

interface BlockchainState {
    assets: Asset[]
    assetPairs: AssetPair[]
}

export default {
    namespaced: true,

    state: (): BlockchainState => ({
        assets: [
        // {
        //     name: 'Tether USD',
        //     symbol: 'USDT',
        //     type: AssetType.ERC20Token,
        //     iconUrl: 'https://exchange.pancakeswap.finance/images/coins/0x55d398326f99059ff775485246999027b3197955.png',
        //     decimals: 18,
        //     blockchain: Blockchain.BSC,
        //     address: '0x55d398326f99059ff775485246999027b3197955',
        // }, {
        //     name: 'USD Coin',
        //     symbol: 'USDC',
        //     type: AssetType.ERC20Token,
        //     iconUrl: 'https://exchange.pancakeswap.finance/images/coins/0x8ac76a51cc950d9822d68b83fe1ad97b32cd580d.png',
        //     decimals: 18,
        //     blockchain: Blockchain.BSC,
        //     address: '0x8ac76a51cc950d9822d68b83fe1ad97b32cd580d',
        // }, {
        //     name: 'BUSD Token',
        //     symbol: 'BUSD',
        //     type: AssetType.ERC20Token,
        //     iconUrl: 'https://exchange.pancakeswap.finance/images/coins/0xe9e7cea3dedca5984780bafc599bd69add087d56.png',
        //     decimals: 18,
        //     blockchain: Blockchain.BSC,
        //     address: '0xe9e7cea3dedca5984780bafc599bd69add087d56',
        // }, {
        //     name: 'Ethereum Token',
        //     symbol: 'ETH',
        //     type: AssetType.ERC20Token,
        //     iconUrl: 'https://exchange.pancakeswap.finance/images/coins/0x2170ed0880ac9a755fd29b2688956bd959f933f8.png',
        //     decimals: 18,
        //     blockchain: Blockchain.BSC,
        //     address: '0x2170ed0880ac9a755fd29b2688956bd959f933f8'
        // }, {
        //     name: 'BTCB Token',
        //     symbol: 'BTCB',
        //     type: AssetType.ERC20Token,
        //     iconUrl: 'https://exchange.pancakeswap.finance/images/coins/0x7130d2a12b9bcbfae4f2634d864a1ee1ce3ead9c.png',
        //     decimals: 18,
        //     blockchain: Blockchain.BSC,
        //     address: '0x7130d2a12b9bcbfae4f2634d864a1ee1ce3ead9c',
        // }, {
        //     name: 'Cardano Token',
        //     symbol: 'ADA',
        //     type: AssetType.ERC20Token,
        //     iconUrl: 'https://exchange.pancakeswap.finance/images/coins/0x3ee2200efb3400fabb9aacf31297cbdd1d435d47.png',
        //     decimals: 18,
        //     blockchain: Blockchain.BSC,
        //     address: '0x3ee2200efb3400fabb9aacf31297cbdd1d435d47',
        // }, {
        //     name: 'TRON',
        //     symbol: 'TRX',
        //     type: AssetType.ERC20Token,
        //     iconUrl: 'https://exchange.pancakeswap.finance/images/coins/0x85eac5ac2f758618dfa09bdbe0cf174e7d574d5b.png',
        //     decimals: 18,
        //     blockchain: Blockchain.BSC,
        //     address: '0x85eac5ac2f758618dfa09bdbe0cf174e7d574d5b',
        // }, {
        //     name: 'PancakeSwap Token',
        //     symbol: 'CAKE',
        //     type: AssetType.ERC20Token,
        //     iconUrl: 'https://exchange.pancakeswap.finance/images/coins/0x0e09fabb73bd3ade0a17ecc321fd13a19e81ce82.png',
        //     decimals: 18,
        //     blockchain: Blockchain.BSC,
        //     address: '0x0e09fabb73bd3ade0a17ecc321fd13a19e81ce82',
        // }, {
        //     name: 'Dogecoin',
        //     symbol: 'DOGE',
        //     type: AssetType.ERC20Token,
        //     iconUrl: 'https://exchange.pancakeswap.finance/images/coins/0xbA2aE424d960c26247Dd6c32edC70B295c744C43.png',
        //     decimals: 8,
        //     blockchain: Blockchain.BSC,
        //     address: '0xba2ae424d960c26247dd6c32edc70b295c744c43',
        // }, {
        //     name: 'ChainLink Token',
        //     symbol: 'LINK',
        //     type: AssetType.ERC20Token,
        //     iconUrl: 'https://exchange.pancakeswap.finance/images/coins/0xf8a0bf9cf54bb92f17374d9e9a321e6a111a51bd.png',
        //     decimals: 8,
        //     blockchain: Blockchain.BSC,
        //     address: '0xf8a0bf9cf54bb92f17374d9e9a321e6a111a51bd',
        // }, {
        //     name: 'United States Dollar',
        //     symbol: 'USD',
        //     type: AssetType.FiatCurrency,
        //     iconUrl: 'https://cdn4.iconfinder.com/data/icons/social-messaging-ui-color-and-shapes-6/177800/295-512.png',
        //     decimals: 2,
        // },
        //
        // {
        //     name: 'Bitcoin',
        //     symbol: 'BTC',
        //     type: AssetType.BlockchainCurrency,
        //     blockchain: Blockchain.BTC,
        //     iconUrl: 'https://exchange.pancakeswap.finance/images/coins/0x7130d2a12b9bcbfae4f2634d864a1ee1ce3ead9c.png',
        //     decimals: 8,
        // }
        ],
        assetPairs: [
        // {
        //     firstSymbol: 'BTC',
        //     secondSymbol: 'USD',
        //     address: '0x5741306c21795fdcbb9b265ea0255f499dfe515c',
        //     decimals: 8,
        // }, {
        //     firstSymbol: 'ETH',
        //     secondSymbol: 'USD',
        //     address: '0x143db3ceefbdfe5631add3e50f7614b6ba708ba7',
        //     decimals: 8,
        // }, {
        //     firstSymbol: 'ADA',
        //     secondSymbol: 'USD',
        //     address: '0x5e66a1775bbc249b5d51c13d29245522582e671c',
        //     decimals: 8,
        // }, {
        //     firstSymbol: 'DOGE',
        //     secondSymbol: 'USD',
        //     address: '0x963d5e7f285cc84ed566c486c3c1bc911291be38',
        //     decimals: 8,
        // }, {
        //     firstSymbol: 'LINK',
        //     secondSymbol: 'USD',
        //     address: '0x1b329402cb1825c6f30a0d92ab9e2862be47333f',
        //     decimals: 8,
        // }
        ]
    }),

    getters: {
        assets(state: BlockchainState): Asset[] {
            return state.assets
        },

        // getAsset: (state: BlockchainState) => (symbol: string): Asset => {
        //     const assets = state.assets.filter(a => a.symbol == symbol)
        //
        //     if (assets.length > 0) {
        //         return assets[0]
        //     }
        //
        //     throw {code: 'assetNotFound'}
        // },
        //
        // getAssetPair: (state: BlockchainState) => (firstSymbol: string, secondSymbol: string): AssetPair => {
        //     const assetPairs = state.assetPairs.filter(
        //         a => firstSymbol == a.firstSymbol && secondSymbol == a.secondSymbol)
        //
        //     if (assetPairs.length > 0) {
        //         return assetPairs[0]
        //     }
        //
        //     throw {code: 'assetPairNotFound'}
        // },
    }
}
