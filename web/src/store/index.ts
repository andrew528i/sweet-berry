import {createStore} from 'vuex'
import VuexPersistence from 'vuex-persist'
import blockchain from './modules/blockchain'

import wallet from './modules/wallet'

export const key = Symbol()

export const store = createStore({
  modules: {
    wallet,
    blockchain,
  },
  plugins: [new VuexPersistence().plugin]
})
