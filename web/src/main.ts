import {createApp} from 'vue'
import {createI18n} from 'vue-i18n'
import Toast, {PluginOptions, POSITION} from 'vue-toastification'
import VueCountdown from '@chenfengyuan/vue-countdown'
import 'vue-toastification/dist/index.css'

import VueApexCharts from 'vue3-apexcharts'
import '../style/main.css'
import '../style/theme.css'

import App from './App.vue'
import messages from './messages'
import router from './router'
import {key, store} from './store'
import directives from './directives/'

// add i18n
const i18n = createI18n({
    legacy: false,
    globalInjection: true,
    locale: 'en',
    fallbackLocale: 'en',
    messages,
})

const app = createApp(App)

directives(app)

if (VueCountdown && VueCountdown.name) app.component(VueCountdown.name, VueCountdown)

app
    .use(store, key)
    .use(router)
    .use(VueApexCharts)
    .use(i18n)
    .use(Toast, { position: POSITION.BOTTOM_RIGHT, timeout: 5000, hideProgressBar: false, icon: true } as PluginOptions)
    .mount('#app')
