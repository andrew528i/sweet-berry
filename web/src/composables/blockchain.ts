import {useWallet} from '@/composables/wallet'
import {config} from '@/config'
import {Round, RoundMeta} from "@/types/blockchain";
import {WalletProvider} from "@/types/wallet";
import {fromBaseUnit} from "@/util";
import {BigNumber} from "ethers";
import {Contract} from 'web3-eth-contract'
import {AbiItem} from 'web3-utils'

// eslint-disable-next-line @typescript-eslint/no-var-requires
const tokenABI: AbiItem[] = require('../assets/abi/erc20.json')
// eslint-disable-next-line @typescript-eslint/no-var-requires
const assetManagerABI: AbiItem[] = require('../assets/abi/assetManager.json')
// eslint-disable-next-line @typescript-eslint/no-var-requires
const betManagerABI: AbiItem[] = require('../assets/abi/betManager.json')
// eslint-disable-next-line @typescript-eslint/no-var-requires
const farmManagerABI: AbiItem[] = require('../assets/abi/farmManager.json')

interface BlockchainComposable {
    getTokenContract: (provider?: WalletProvider) => Contract
    getCustomTokenContract: (address: string, provider?: WalletProvider) => Contract
    getAssetManagerContract: () => Contract
    getBetManagerContract: (provider?: WalletProvider) => Contract
    getFarmManagerContract: (provider?: WalletProvider) => Contract
    calculateRoundLocked: (round: Round) => string
    calculateRoundPriceChange: (roundMeta: RoundMeta) => string
    shortAddress: (address?: string) => string | null
}

export function useBlockchain(): BlockchainComposable {
    const getTokenContract = (provider?: WalletProvider): Contract => {
        const web3 = useWallet().getWeb3(provider)

        return new web3.eth.Contract(tokenABI, config.token.address)
    }

    const getCustomTokenContract = (address: string, provider?: WalletProvider): Contract => {
        const web3 = useWallet().getWeb3(provider)

        return new web3.eth.Contract(tokenABI, address)
    }

    const getAssetManagerContract = (): Contract => {
        const web3 = useWallet().getWeb3()

        return new web3.eth.Contract(assetManagerABI, config.assetManager.address)
    }

    const getBetManagerContract = (provider?: WalletProvider): Contract => {
        const web3 = useWallet().getWeb3(provider)

        return new web3.eth.Contract(betManagerABI, config.betManager.address)
    }

    const getFarmManagerContract = (provider?: WalletProvider): Contract => {
        const web3 = useWallet().getWeb3(provider)

        return new web3.eth.Contract(farmManagerABI, config.farmManager.address)
    }

    const calculateRoundLocked = (round: Round): string => {
        return fromBaseUnit(BigNumber.from(round.upBetsValue).add(BigNumber.from(round.downBetsValue)), config.token.decimals, 2).toString()
    }

    const calculateRoundPriceChange = (roundMeta: RoundMeta): string => {
        return (100 * (roundMeta.priceTo - roundMeta.priceFrom) / roundMeta.priceFrom).toFixed(2)
    }

    const shortAddress = (address?: string): string | null => {
        if (!address) return null

        return address.substr(0, 6) + '...' + address.substr(-4)
    }

    return {
        getTokenContract, getCustomTokenContract, getAssetManagerContract, getBetManagerContract,
        getFarmManagerContract, calculateRoundLocked, calculateRoundPriceChange, shortAddress}
}
