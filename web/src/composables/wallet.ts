import {config} from '@/config'
import {WalletProvider, WalletType} from '@/types/wallet'
import Web3 from 'web3'
import {provider as web3Provider} from 'web3-core'

interface WalletComposable {
    getWeb3: (provider?: WalletProvider) => Web3
    getPublicWeb3: () => Web3
    getWeb3Address: (p: WalletProvider) => Promise<string>
}

declare global {
    interface Window {
        ethereum: web3Provider
        BinanceChain: web3Provider
    }
}

export function useWallet(): WalletComposable {
    const getWeb3 = (provider?: WalletProvider): Web3 => {
        if (!provider) return getPublicWeb3()

        if (provider.type == WalletType.MetaMask) return new Web3(window.ethereum)
        if (provider.type == WalletType.Binance) return new Web3(window.BinanceChain)

        return getPublicWeb3()
    }

    const getPublicWeb3 = (): Web3 => (new Web3(new Web3.providers.HttpProvider(config.providerUrl)))

    const getWeb3Address = async (provider: WalletProvider): Promise<string> => {
        const accounts = await getWeb3(provider).eth.requestAccounts()
        if (accounts.length > 0) return accounts[0]
        throw {code: 'noAddresses'}
    }

    return { getWeb3, getPublicWeb3, getWeb3Address }
}
