import Web3 from 'web3';
interface WalletComposable {
    getWeb3: () => Web3 | null;
    getAddress: () => Promise<string | null>;
}
export declare function useWallet(): WalletComposable;
export {};
