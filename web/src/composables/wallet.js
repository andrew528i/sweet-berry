import useWalletState from '@/store/useWalletStore';
import Web3 from 'web3';
export function useWallet() {
    const { walletProvider } = useWalletState();
    const getWeb3 = () => {
        if (!walletProvider) {
            return null;
        }
        switch (walletProvider.type) {
            case WalletType.MetaMask:
                return new Web3(window.ethereum);
            case WalletType.Binance:
                return new Web3(window.BinanceChain);
            default:
                return null;
        }
    };
    const getAddress = async () => {
        const web3 = getWeb3();
        if (web3 === null) {
            return null;
        }
        const accounts = await web3.eth.requestAccounts();
        if (accounts.length > 0) {
            return accounts[0];
        }
        return null;
    };
    return { getWeb3, getAddress };
}
