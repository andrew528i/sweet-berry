import mitt, {Emitter} from 'mitt'

const emitter = mitt()

export function useEmitter(): Emitter {
    return emitter
}
