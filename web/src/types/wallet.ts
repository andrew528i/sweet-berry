// export namespace Wallet {
//     declare enum Type {
//         MetaMask,
//         Binance,
//     }
//
//     declare interface Provider {
//         name: string
//         iconUrl?: string
//         type: Type
//     }
// }

// declare global {
//     enum WalletType {
//         MetaMask,
//         Binance,
//     }
//
//     interface WalletProvider {
//         name: string
//         iconUrl?: string
//         type: WalletType
//     }
// }

export enum WalletType {
    MetaMask,
    Binance,
}

export interface WalletProvider {
    name: string
    iconUrl?: string
    type: WalletType
}

export {}
