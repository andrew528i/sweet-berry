export enum AssetType {Token, Native, Stock, Fiat}
export enum Blockchain {SmartChain, Ethereum, Bitcoin, Tron}
export enum AssetPairState {Inactive, Active, TemporarySuspended, Suspended}
export enum BetDirection {Up, Down}

// export interface Asset {
//     name: string
//     symbol: string
//     iconUrl: string
//     decimals: number
//     type: AssetType
//     blockchain?: Blockchain
//     address?: string
// }
//
// export interface AssetPair {
//     firstSymbol: string
//     secondSymbol: string
//     address: string
//     decimals: number
// }

export interface Asset {
    symbol: string
    decimals: number
    type_: AssetType
    token: string
    blockchain: Blockchain
}

export interface AssetPair {
    first: Asset
    second: Asset
    priceFeed: string
    state: AssetPairState
    createdAt: number
    roundDuration: number
    maxBet: number
    index: number
}

export interface Round {
    linkRoundIdFrom: number
    linkTimestampFrom: number
    linkPriceFrom: number

    linkRoundIdTo: number
    linkTimestampTo: number
    linkPriceTo: number

    upBetsValue: number
    upBetsCount: number
    downBetsValue: number
    downBetsCount: number
}

export interface PriceTick {
    timestamp: number
    price: number
}

export interface RoundMeta {
    round: Round
    assetPair: AssetPair
    progress: number
    roundId: number
    price: number
    decimals: number
    history: PriceTick[]
    upCoef: number
    downCoef: number
    pendingReward: number
    timestampFrom: number
    timestampTo: number
    upBetsCount: number
    downBetsCount: number
    priceFrom: number
    priceTo: number
}

export interface Bet {
    value: number
    direction: BetDirection
    createdAt: number
    rewardValue: number
}

// farms
export interface Tag {
    text: string
    class?: string
}

export interface Farm {
    depositToken: string
    allocPoint: number
    lastRewardBlock: number
    accERC20PerShare: number
    lockTime: number
    blockFrom: number
    blockTo: number
    paidOut: number
    rewardPerBlock: number
}

export interface FarmMeta {
    farm: Farm
    farmId: number
    progress: number
    deposited: number
    balance: number
    supply: number
    tokenSymbol: string
    tokenName: string
    tokenDecimals: number
    tokenAddress: string
    unlockTime: number
    pendingReward: number
    remainReward: number
}

export interface PriceMetaInfo {
    upCount: number
    downCount: number
    volatileCount: number
    highCoefCount: number
    currentRoundValue: number
    pairsCount: number
    tokenPrice: number
}

export interface FarmMetaInfo {
    totalSupply: number
    currentSupply: number
    rewardPerBlock: number
    farmCount: number
    tokenPrice: number
}

export {}
