import { createRouter, createWebHistory } from 'vue-router';
const routes = [
    { path: '/', name: 'Home', component: () => import('../pages/Index.vue') },
    { path: '/about', name: 'About', component: () => import('../pages/About.vue') }
];
const router = createRouter({
    history: createWebHistory(process.env.BASE_URL),
    routes
});
export default router;
