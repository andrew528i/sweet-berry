import { createRouter, createWebHistory, RouteRecordRaw } from 'vue-router'

const routes: Array<RouteRecordRaw> = [
  { path: '/', redirect: '/price' },
  { path: '/price', name: 'Price', component: () => import('../pages/Price.vue') },
  { path: '/farm', name: 'Farm', component: () => import('../pages/Farm.vue') }
]

const router = createRouter({
  history: createWebHistory(process.env.BASE_URL),
  routes
})

export default router
