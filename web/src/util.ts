import {useEmitter} from "@/composables/emitter";
import {config} from '@/config'
import animate from "animejs";
import {BigNumber, FixedNumber} from "ethers";
import {onMounted, Ref} from 'vue'
import {useToast} from "vue-toastification";
import Web3 from "web3";
import {TransactionReceipt} from "web3-core";

type Callback = () => void

export function onLoaded(callback: Callback): void {
    if (document.readyState === 'complete') {
        callback()
    } else window.addEventListener('load', () => {
        callback()
    })
}

export function onMountedAndLoaded(callback: Callback): void { onMounted(() => onLoaded(callback)) }


export function getIconBySymbol(symbol: string): string {
    const tokenMeta = config.assetMeta[symbol]

    if (!tokenMeta) return '@/assets/image/unknown.png'

    return tokenMeta.iconUrl
}

export function syncBlockchain(): void {
    useEmitter().emit(config.events.SYNC_BLOCKCHAIN)
}

export function onSyncBlockchain(callback: Callback): void {
    useEmitter().on(config.events.SYNC_BLOCKCHAIN, callback)
}

export function handleError(error: Error): void {
    useToast().error(error.message)
    useEmitter().emit(config.events.ERROR, error)
    console.error(error)
}

export function waitForTransaction(web3: Web3, txHash: string): Promise<TransactionReceipt> {
    const interval = 1000

    const getReceipt = async (
        resolve: (result: TransactionReceipt) => void,
        reject: (error: Error) => void,
    ) => {
        const receipt = await web3.eth.getTransactionReceipt(txHash)

        if (receipt) {
            resolve(receipt)
        } else setTimeout(() => getReceipt(resolve, reject), interval)
    }

    return new Promise(getReceipt)
}

export function fromBaseUnit(value: number | string | BigNumber, decimals: number, roundDecimals?: number): FixedNumber | string {
    let bnValue = FixedNumber.from(value)

    bnValue = bnValue.divUnsafe(FixedNumber.from(BigNumber.from(10).pow(decimals)))

    if (roundDecimals) {
        return parseFloat(bnValue.toString()).toFixed(roundDecimals)
    }

    return bnValue
}

export function toBaseUnit(value: BigNumber | number | string, decimals: number): BigNumber {
    const baseValue = BigNumber.from(10).pow(decimals)
    // let bnValue;

    if (value < 1) {
        const bnValue = BigNumber.from(1 / parseFloat(value.toString()))

        return baseValue.div(bnValue)
    }

    return BigNumber.from(FixedNumber.fromString(value.toString()).mulUnsafe(FixedNumber.from(baseValue)).toString().split('.')[0])
}

export function animateValue(target: Ref<number> | string, value: number, easing?: string): void {
    animate({
        targets: target,
        value,
        round: 1,
        duration: 500,
        easing: easing || 'easeInBounce',
        delay: 250,
    })
}

export function leadingZero(n: number | string): string {
    if (n.toString().length < 2) return '0' + n

    return n.toString()
}
